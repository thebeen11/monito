import { OurProducts } from "@/model/OurProducts.model";
import productData from "@/dummy-data/ourProducts.json";
import { defineStore } from "pinia";

interface OurProductStore{
  ourProduct: OurProducts[]
  loading: boolean
}

export const useOurProductStore = defineStore('useOurProductStore', {
  state:():OurProductStore=>({
    ourProduct: [],
    loading: false
  }),
  actions:{
    async getDataProduct(){
      try {
        this.loading = true
        this.ourProduct = productData
        this.loading = false
      } catch (error) {
        this.loading = false
        console.log(error);
        
      }
    }
  }
})