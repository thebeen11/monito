import { OurPet } from "@/model/OurPets.model";
import petData from "@/dummy-data/ourPets.json";
import { defineStore } from "pinia";

interface OurPetsStore{
  ourPets: OurPet[]
  loading: boolean
}

export const useOurPetsStore = defineStore('useOurPetsStore', {
  state:():OurPetsStore=>({
   ourPets: [],
   loading: false
  }),
  actions:{
    async getDataPets(){
      try {
        this.loading = true
        this.ourPets = petData
        this.loading = false
      } catch (error) {
        this.loading = false
        console.log(error);
        
      }
    }
  }
})