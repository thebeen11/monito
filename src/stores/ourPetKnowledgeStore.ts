import { PetKnowledge } from "@/model/OurPetKnwledge.model";
import petKnowledgeData from "@/dummy-data/ourPetKnowledge.json";
import { defineStore } from "pinia";

interface OurpetKnowledgeStore{
  petKnowledge: PetKnowledge[]
  loading: boolean
}

export const useOurpetKnowledgeStore = defineStore('useOurpetKnowledgeStore', {
  state:():OurpetKnowledgeStore=>({
    petKnowledge: [],
    loading: false
  }),
  actions:{
    async getDataPetsKnowledge(){
      try {
        this.loading = true
        this.petKnowledge = petKnowledgeData
        this.loading = false
      } catch (error) {
        this.loading = false
        console.log(error);
        
      }
    }
  }
})