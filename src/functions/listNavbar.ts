import { Menu } from "@/model/navbar.model";

export const listNavbar: Menu[] = [
  { name: "Home", path: "/" },
  { name: "Category", path: "#" },
  { name: "About", path: "#" },
  { name: "Contact", path: "#" },
];

export const listMediaSocial: Menu[] = [
  { name: "Facebook - Negative.svg", path: "facebook.com" },
  { name: "Instagram - Negative.svg", path: "instagram.com" },
  { name: "Twitter - Negative.svg", path: "twitter.com" },
  { name: "YouTube - Negative.svg", path: "youtube.com" },
];

