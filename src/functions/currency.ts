export function formatCurrency(amount: number): string {
    // Format the integer part with commas
    const formattedIntegerPart = String(amount).replace(/\B(?=(\d{3})+(?!\d))/g, '.');

    // Combine the integer part and the fractional part with the currency symbol
    return `${formattedIntegerPart} VND`; // đ is the currency symbol for Vietnamese dong
}
