export interface OurPet {
    img:   string;
    title: string;
    specs: Spec[];
    price: number;
}

export interface Spec {
    name:  string;
    value: string;
}
