import { Spec } from "./OurPets.model";

export interface OurProducts {
    img:   string;
    title: string;
    specs: Spec[];
    price: number;
    badge?: string
}