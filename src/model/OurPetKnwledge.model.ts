export interface PetKnowledge {
    img:   string;
    title: string;
    desc:  string;
}
